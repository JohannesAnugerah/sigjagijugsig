<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLaundryForeignLokasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laundry', function (Blueprint $table) {
            //
            $table->integer('lokasi_laundry')->nullable();
            $table->foreign('lokasi_laundry')
                ->references('id')
                ->on('lokasi')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laundry', function (Blueprint $table) {
            //
            $table->dropForeign('laundry_lokasi_laundry_foreign');
            $table->dropColumn('lokasi_laundry');
        });
    }
}
