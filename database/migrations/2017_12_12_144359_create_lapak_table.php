<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatelapakTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lapak', function (Blueprint $table) {
            $table -> string('wilayah_kota');
            $table -> string('nama');
            $table -> string('alamat');
            $table -> string('wilayah');
            $table -> string('nama_lokasi');
            $table -> string('masakan_1');
            $table -> string('masakan_2');
            $table -> string('waktu_buka');
            $table -> decimal('latitude');
            $table -> decimal('longitude');
            $table -> increments('id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lapak');
    }
}
