<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Laundry::class, function (Faker\Generator $faker) {
    return [
        'nama' => $faker->name,
        'no_telepon' => $faker->text,
        'harga' => $faker->numberBetween($min = 4000, $max = 7000),
        'durasi' => $faker->numberBetween($min = 1, $max = 3),
        'jam_buka' => '08:00:00',
        'jam_tutup' => '22:00:00',
    ];
});

$factory->define(App\Lokasi::class, function (Faker\Generator $faker) {
    return [
        'latitude' => $faker->latitude(-6.150221, -6.330690),
        'longitude' => $faker->longitude(106.688138, 106.918232),
    ];
});