@extends('layouts.app')
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

               <section id="contact" class="section-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="ser-title">Daftarkan Tempat Laundry</h2>
                                <hr class="botm-line">
                            </div>
                            <div class="col-md-7 col-sm-8 marb20">
                                <div class="contact-info">
                                    <div class="space"></div>
                                    <div id="sendmessage">Tempat laundry berhasil ditambahkan!</div>
                                    <div id="errormessage"></div>
                                    <form action="{{action('LaundryController@store')}}" method="post" role="form">
                                    {{ csrf_field() }}
                                        <div class="form-group">
                                            <input type="text" name="nama" class="form-control br-radius-zero" id="nama" placeholder="Nama Tempat Laundry" data-rule="minlen:1" data-msg="Nama tempat laundry tidak boleh kosong" value="{{ old('nama') }}" />
                                            <div class="validation"></div>
                                        </div>
                                        <div class="form-group">
                                        <input id="pac-input" class="controls" type="text" placeholder="SearchBox">
                                        <div id="map"></div>
                                        <script>
                                              // This example adds a search box to a map, using the Google Place Autocomplete
                                              // feature. People can enter geographical searches. The search box will return a
                                              // pick list containing a mix of places and predicted search terms.

                                              // This example requires the Places library. Include the libraries=places
                                              // parameter when you first load the API. For example:
                                              // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

                                              function initAutocomplete() {
                                                var uluru = {lat: -6.44, lng: 107.444};
                                                var map = new google.maps.Map(document.getElementById('map'), {
                                                    center: uluru,
                                                    zoom: 13,
                                                    mapTypeId: 'roadmap'
                                                    });
                                                var marker = new google.maps.Marker({
                                                    position: uluru,
                                                    map: map,
                                                    draggable: true,
                                                });

                                                google.maps.event.addListener(marker, 'dragend', function(evt) {
                                                    document.getElementById("latitude").value = marker.getPosition().lat();
                                                    document.getElementById("longitude").value = marker.getPosition().lng();
                                                });

                                                // Create the search box and link it to the UI element.
                                                var input = document.getElementById('pac-input');
                                                var searchBox = new google.maps.places.SearchBox(input);
                                                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                                                // Bias the SearchBox results towards current map's viewport.
                                                map.addListener('bounds_changed', function() {
                                                  searchBox.setBounds(map.getBounds());
                                                });

                                                var markers = [];
                                                // Listen for the event fired when the user selects a prediction and retrieve
                                                // more details for that place.
                                                searchBox.addListener('places_changed', function() {
                                                  var places = searchBox.getPlaces();

                                                  if (places.length == 0) {
                                                    return;
                                                  }

                                                  // Clear out the old markers.
                                                  markers.forEach(function(marker) {
                                                    marker.setMap(null);
                                                  });
                                                  markers = [];

                                                  // For each place, get the icon, name and location.
                                                  var bounds = new google.maps.LatLngBounds();
                                                  places.forEach(function(place) {
                                                    if (!place.geometry) {
                                                      console.log("Returned place contains no geometry");
                                                      return;
                                                    }
                                                    var icon = {
                                                      url: place.icon,
                                                      size: new google.maps.Size(71, 71),
                                                      origin: new google.maps.Point(0, 0),
                                                      anchor: new google.maps.Point(17, 34),
                                                      scaledSize: new google.maps.Size(25, 25)
                                                    };

                                                    var temp = new google.maps.Marker({
                                                      map: map,
                                                      icon: icon,
                                                      title: place.name,
                                                      position: place.geometry.location,
                                                      draggable: true,
                                                    })
                                                    document.getElementById("latitude").value = temp.getPosition().lat();
                                                    document.getElementById("longitude").value = temp.getPosition().lng();
                                                    // Create a marker for each place.
                                                    markers.push(temp);
                                                    if (place.geometry.viewport) {
                                                      // Only geocodes have viewport.
                                                      bounds.union(place.geometry.viewport);
                                                    } else {
                                                      bounds.extend(place.geometry.location);
                                                    }
                                                  });
                                                  map.fitBounds(bounds);
                                                });
                                              }

                                            </script>
                                        <div class="form-group">
                                            <input type="decimal" name="latitude" class="form-control br-radius-zero" id="latitude" placeholder="Latitude" data-rule="minlen:1" value="{{ old('latitude') }}" />
                                            <div class="validation"></div>
                                        </div>
                                        <div class="form-group">
                                            <input type="decimal" name="longitude" class="form-control br-radius-zero" id="longitude" placeholder="Longitude" data-rule="minlen:1" value="{{ old('longitude') }}" />
                                            <div class="validation"></div>
                                        </div>
                                        <script async defer
                                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBaq7NEhuWDsBz5nGimwM4eu4XlNNEiU10&callback=initAutocomplete&libraries=places&sensor=false">
                                        </script>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="no_telepon" class="form-control br-radius-zero" id="no_telepon" placeholder="Nomor Telepon Tempat Laundry" data-rule="minlen:1" data-msg="Nomor telepon tempat laundry tidak boleh kosong" value="{{ old('no_telepon') }}" />
                                            <div class="validation"></div>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" name="harga" class="form-control br-radius-zero" id="harga" placeholder="Harga Laundry" data-rule="minlen:1" data-msg="Harga tempat laundry tidak boleh kosong" value="{{ old('harga') }}" />
                                            <div class="validation"></div>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" name="durasi" class="form-control br-radius-zero" id="durasi" placeholder="Durasi Laundry dalam Hari" data-rule="minlen:1" data-msg="Durasi tempat laundry tidak boleh kosong" value="{{ old('durasi') }}" />
                                            <div class="validation"></div>
                                        </div>
                                        <div class="form-group">
                                            <h5>Jam Buka</h5>
                                            <input type="time" name="jam_buka" class="form-control br-radius-zero" id="jam_buka" placeholder="Jam Buka Tempat Laundry" data-rule="minlen:1" data-msg="Jam buka tempat laundry tidak boleh kosong" value="{{ old('jam_buka') }}" />
                                            <div class="validation"></div>
                                        </div>
                                        <div class="form-group">
                                            <h5>Jam Tutup</h5>
                                            <input type="time" name="jam_tutup" class="form-control br-radius-zero" id="jam_tutup" placeholder="Jam Tutup Tempat Laundry" data-rule="minlen:1" data-msg="Jam tutup tempat laundry tidak boleh kosong" value="{{ old('jam_tutup') }}" />
                                            <div class="validation"></div>
                                        </div>
                                        <div class="form-action">
                                            <button type="submit" class="btn btn-form">Daftarkan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@endsection