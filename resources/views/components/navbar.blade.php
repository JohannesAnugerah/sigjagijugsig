<nav class="navbar navbar-default navbar-fixed-top">   
    <div class="container">
        <div class="col-md-12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{action('PageController@index')}}"><img class="img-responsive2"       
           src={{ asset('img/NG-Kost.png') }}></a>
            </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>
        </div>
            
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{ url('/') }}">Home</a></li>
                    {{--<li class=""><a href="{{action('PageController@about')}}">About</a></li>--}}
                    <!-- Authentication Links -->
                </ul>
            </div>
        </div>
    </div>
</nav>