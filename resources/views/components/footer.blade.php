<footer id="footer">
    <div class="footer-line">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    © Copyright KEMEKUY! 2017. All Rights Reserved
                    <div class="credits">
                        <!--
                          All the links in the footer should remain intact.
                          You can delete the links only if you purchased the pro version.
                          Licensing information: https://bootstrapmade.com/license/
                          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Medilab
                        -->
                        Designed by <a href="{{action('PageController@about')}}">KEMEKUY!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
