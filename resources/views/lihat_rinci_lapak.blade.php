
@extends('layouts.app')

@section('content')
    <section id="banner" class="banner">
        <div class="bg-color">
            <div class="container">
                <div class="row">
                    <div class="banner-info">
                        <div class="banner-text text-center">
                            <h1 class="white">KEMEKUY!</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="service" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h2 class="ser-title">{{$nama}}</h2>
                    <hr class="botm-line">
                    <p>
                        Masakan 1 : {{$masakan_1}}
                    </p>
                    <p>
                        Masakan 2 : {{$masakan_2}}
                    </p>
                    <p>
                        Jam buka-tutup : {{$waktu_buka}}
                    </p>
                    <p>
                        Alamat         : {{$alamat}}, {{$wilayah}}, {{$nama_lokasi}}, {{$wilayah_kota}}
                    </p>
                </div>

                <html>
                  <head>
                    <style>
                       /* Set the size of the div element that contains the map */
                      #map {
                        height: 400px;  /* The height is 400 pixels */
                        width: 50%;  /* The width is the width of the web page */
                       }
                    </style>
                  </head>
                  <body>
                    <h3>Berikut Lokasinya</h3>
                    <!--The div element for the map -->
                    <div id="map"></div>
                    <script>
                // Initialize and add the map
                function initMap() {
                  // The location of Uluru
                  var uluru = {lat: {{$latitude}}, lng: {{$longitude}}};
                  // The map, centered at Uluru
                  var map = new google.maps.Map(
                      document.getElementById('map'), {zoom: 15, center: uluru});
                  // The marker, positioned at Uluru
                  var marker = new google.maps.Marker({position: uluru, map: map});
                }
                    </script>
                    <!--Load the API from the specified URL
                    * The async attribute allows the browser to render the page while the API loads
                    * The key parameter will contain your own API key (which is not needed for this tutorial)
                    * The callback parameter executes the initMap() function
                    -->
                    <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8PXaqrfSTGtnwbMzssvytXbL4s3bYbdg&callback=initMap">
                    </script>
                  </body>
                </html>

               <!--  <html>
                  <head>
                    <title>Geolocation</title>
                    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
                    <meta charset="utf-8">
                    <style>
                      /* Always set the map height explicitly to define the size of the div
                       * element that contains the map. */
                      #map {
                        height: 100%;
                      }
                      /* Optional: Makes the sample page fill the window. */
                      html, body {
                        height: 100%;
                        margin: 0;
                        padding: 0;
                      }
                    </style>
                  </head>
                  <body>
                    <div id="map"></div>
                    <script>
                      // Note: This example requires that you consent to location sharing when
                      // prompted by your browser. If you see the error "The Geolocation service
                      // failed.", it means you probably did not give permission for the browser to
                      // locate you.
                      var map, infoWindow;
                      function initMap() {
                        map = new google.maps.Map(document.getElementById('map'), {
                          center: {lat: -34.397, lng: 150.644},
                          zoom: 6
                        });
                        infoWindow = new google.maps.InfoWindow;

                        // Try HTML5 geolocation.
                        if (navigator.geolocation) {
                          navigator.geolocation.getCurrentPosition(function(position) {
                            var pos = {
                              lat: position.coords.latitude,
                              lng: position.coords.longitude
                            };

                            infoWindow.setPosition(pos);
                            infoWindow.setContent('Location found.');
                            infoWindow.open(map);
                            map.setCenter(pos);
                          }, function() {
                            handleLocationError(true, infoWindow, map.getCenter());
                          });
                        } else {
                          // Browser doesn't support Geolocation
                          handleLocationError(false, infoWindow, map.getCenter());
                        }
                      }

                      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                        infoWindow.setPosition(pos);
                        infoWindow.setContent(browserHasGeolocation ?
                                              'Error: The Geolocation service failed.' :
                                              'Error: Your browser doesn\'t support geolocation.');
                        infoWindow.open(map);
                      }
                    </script>
                    <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8PXaqrfSTGtnwbMzssvytXbL4s3bYbdg&callback=initMap">
                    </script>
                  </body>
                </html> -->
                <!-- <div class="col-md-6 col-sm-6">
                    <div id="map"></div>
                    <script>
                      function initMap() {
                        var uluru = {lat: {{$latitude}}, lng: {{$longitude}}};
                        var map = new google.maps.Map(document.getElementById('map'), {
                          zoom: 17,
                          center: uluru
                        });
                        var marker = new google.maps.Marker({
                          position: uluru,
                          map: map,
                          title: 'Hello World!'
                        });
                      }
                    </script>
                    <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8PXaqrfSTGtnwbMzssvytXbL4s3bYbdg&callback=initMap">
                    // src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJHPBDwL_0bXgAEwjOHF-Vuii-tK_-2fY&callback=initMap">
                    </script>
                </div> -->
            </div>
        </div>
    </section>
@endsection
