
@extends('layouts.app')

@section('content')
    <section id="banner" class="banner">
        <div class="bg-color">
            <div class="container">
                <div class="row">
                    <div class="banner-info">
                        <div class="banner-text text-center">
                            <h1 class="white">KEMEKUY!</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="service" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <h2 class="ser-title">{{$title}}</h2>
                    <hr class="botm-line">
                    <p>Lokasi lapak terdekat dengan lokasi Anda berdasarkan IP address. <br>IP address Anda: {{$ip}}<br>Latitude Anda berada: {{$lat}}<br>Longitude Anda berada: {{$lng}}<br>Kota Anda berada: {{$cityName}}<br>Daerah Anda berada: {{$regionName}}<br>Negara Anda berada: {{$countryName}}</p>
                </div>

                <div class="col-md-8 col-sm-8">
                    <div class="service-info">
                        <div class="icon">
                        <p><strong>Radius :</strong></p> 
                        <div class="icon">
                        <a href="{{ url('/lapak2') }}" class="btn btn-default">2 km</a>
                        <a href="{{ url('/lapak5') }}" class="btn btn-default">5 km</a>
                        <a href="{{ url('/lapak10') }}" class="btn btn-default">10 km</a>
                        <a href="{{ url('/lapak') }}" class="btn btn-default">Semua Lapak</a>
                        <br>
                        <br>
                        
                        <div class="icon-info">
                            <style>
                                .clickable{
                                    cursor: pointer;
                                }

                                .panel-heading div {
                                    margin-top: -18px;
                                    font-size: 15px;
                                }
                                .panel-heading div span{
                                    margin-left:5px;
                                }
                                .panel-body{
                                    display: none;
                                }
                            </style>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">lapak</h3>
                                    <div class="pull-right">
							<span class="clickable filter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
								<i class="glyphicon glyphicon-filter"></i>
							</span>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter lapak" />
                                </div>
                                <table class="table table-hover" id="dev-table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Lapak</th>
                                        <th>Waktu Buka</th>
                                        <th>Radius (km)</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $num = 1; ?>
                                    @foreach($lapak as $lpk)
                                        @if($lpk->distance < 2) 
                                            <tr>
                                                <td>{{$num++}}</td>
                                                <td>{{$lpk->nama}}</td>
                                                <td>{{$lpk->waktu_buka}}</td>
                                                <td>{{$lpk->distance}}<td>
                                                <td><a href="{{action('PageController@detail_lapak', $lpk->id)}}">Detail</a></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <script>
                                /**
                                 *   I don't recommend using this plugin on large tables, I just wrote it to make the demo useable. It will work fine for smaller tables
                                 *   but will likely encounter performance issues on larger tables.
                                 *
                                 *		<input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Developers" />
                                 *		$(input-element).filterTable()
                                 *
                                 *	The important attributes are 'data-action="filter"' and 'data-filters="#table-selector"'
                                 */
                                (function(){
                                    'use strict';
                                    var $ = jQuery;
                                    $.fn.extend({
                                        filterTable: function(){
                                            return this.each(function(){
                                                $(this).on('keyup', function(e){
                                                    $('.filterTable_no_results').remove();
                                                    var $this = $(this),
                                                        search = $this.val().toLowerCase(),
                                                        target = $this.attr('data-filters'),
                                                        $target = $(target),
                                                        $rows = $target.find('tbody tr');

                                                    if(search == '') {
                                                        $rows.show();
                                                    } else {
                                                        $rows.each(function(){
                                                            var $this = $(this);
                                                            $this.text().toLowerCase().indexOf(search) === -1 ? $this.hide() : $this.show();
                                                        })
                                                        if($target.find('tbody tr:visible').size() === 0) {
                                                            var col_count = $target.find('tr').first().find('td').size();
                                                            var no_results = $('<tr class="filterTable_no_results"><td colspan="'+col_count+'">No results found</td></tr>')
                                                            $target.find('tbody').append(no_results);
                                                        }
                                                    }
                                                });
                                            });
                                        }
                                    });
                                    $('[data-action="filter"]').filterTable();
                                })(jQuery);

                                $(function(){
                                    // attach table filter plugin to inputs
                                    $('[data-action="filter"]').filterTable();

                                    $('.container').on('click', '.panel-heading span.filter', function(e){
                                        var $this = $(this),
                                            $panel = $this.parents('.panel');

                                        $panel.find('.panel-body').slideToggle();
                                        if($this.css('display') != 'none') {
                                            $panel.find('.panel-body input').focus();
                                        }
                                    });
                                    $('[data-toggle="tooltip"]').tooltip();
                                })
                            </script>
                        </div>
                        <a href="{{ action('PageController@all_lapak') }}" class="btn btn-default">Lihat semua tempat lapak</a>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
