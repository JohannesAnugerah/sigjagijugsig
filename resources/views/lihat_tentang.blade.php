@extends('layouts.app')

@section('content')
    <section id="banner" class="banner">
        <div class="bg-color">
            <div class="container">
                <div class="row">
                    <div class="banner-info">
                        <div class="banner-text text-center">
                            <h1 class="white">NG-KOST</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<!--service-->
<section id="service" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <h2 class="ser-title">Our Service</h2>
                <hr class="botm-line">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris cillum.</p>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="service-info">
                    <div class="icon">
                        <i class="fa fa-stethoscope"></i>
                    </div>
                    <div class="icon-info">
                        <h4>24 Hour Support</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
                <div class="service-info">
                    <div class="icon">
                        <i class="fa fa-ambulance"></i>
                    </div>
                    <div class="icon-info">
                        <h4>Emergency Services</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="service-info">
                    <div class="icon">
                        <i class="fa fa-user-md"></i>
                    </div>
                    <div class="icon-info">
                        <h4>Medical Counseling</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
                <div class="service-info">
                    <div class="icon">
                        <i class="fa fa-medkit"></i>
                    </div>
                    <div class="icon-info">
                        <h4>Premium Healthcare</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ service-->

<!--about-->
<section id="about" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="section-title">
                    <h2 class="head-title lg-line">The Medilap <br>Ultimate Dream</h2>
                    <hr class="botm-line">
                    <p class="sec-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..</p>
                    <a href="" style="color: #0cb8b6; padding-top:10px;">Know more..</a>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <div style="visibility: visible;" class="col-sm-9 more-features-box">
                    <div class="more-features-box-text">
                        <div class="more-features-box-text-icon"> <i class="fa fa-angle-right" aria-hidden="true"></i> </div>
                        <div class="more-features-box-text-description">
                            <h3>It's something important you want to know.</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. Ut wisi enim ad minim veniam, quis nostrud.</p>
                        </div>
                    </div>
                    <div class="more-features-box-text">
                        <div class="more-features-box-text-icon"> <i class="fa fa-angle-right" aria-hidden="true"></i> </div>
                        <div class="more-features-box-text-description">
                            <h3>It's something important you want to know.</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. Ut wisi enim ad minim veniam, quis nostrud.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ about-->

<!--doctor team-->
<section id="doctor-team" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="ser-title">Meet Our Doctors!</h2>
                <hr class="botm-line">
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="thumbnail">
                    <img src="img/doctor1.jpg" alt="..." class="team-img">
                    <div class="caption">
                        <h3>Jessica Wally</h3>
                        <p>Doctor</p>
                        <ul class="list-inline">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="thumbnail">
                    <img src="img/doctor2.jpg" alt="..." class="team-img">
                    <div class="caption">
                        <h3>Iai Donas</h3>
                        <p>Doctor</p>
                        <ul class="list-inline">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="thumbnail">
                    <img src="img/doctor3.jpg" alt="..." class="team-img">
                    <div class="caption">
                        <h3>Amanda Denyl</h3>
                        <p>Doctor</p>
                        <ul class="list-inline">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="thumbnail">
                    <img src="img/doctor4.jpg" alt="..." class="team-img">
                    <div class="caption">
                        <h3>Jason Davis</h3>
                        <p>Doctor</p>
                        <ul class="list-inline">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ doctor team-->
<!--testimonial-->
<section id="testimonial" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="ser-title">see what patients are saying?</h2>
                <hr class="botm-line">
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="testi-details">
                    <!-- Paragraph -->
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
                <div class="testi-info">
                    <!-- User Image -->
                    <a href="#"><img src="img/thumb.png" alt="" class="img-responsive"></a>
                    <!-- User Name -->
                    <h3>Alex<span>Texas</span></h3>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="testi-details">
                    <!-- Paragraph -->
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
                <div class="testi-info">
                    <!-- User Image -->
                    <a href="#"><img src="img/thumb.png" alt="" class="img-responsive"></a>
                    <!-- User Name -->
                    <h3>Alex<span>Texas</span></h3>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="testi-details">
                    <!-- Paragraph -->
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
                <div class="testi-info">
                    <!-- User Image -->
                    <a href="#"><img src="img/thumb.png" alt="" class="img-responsive"></a>
                    <!-- User Name -->
                    <h3>Alex<span>Texas</span></h3>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ testimonial-->

@endsection
