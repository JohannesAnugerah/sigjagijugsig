
@extends('layouts.app')
@section('content')
    <!--banner-->
    <section id="banner" class="banner">
    <div class="bg-color">
    <div class="container">
        <div class="row">
            <div class="banner-info">
                <div class="banner-logo text-center">
                </div>
                <div class="banner-text text-center">
                    <h1 class="white">Cari lapak sesuai seleramu <br> KEMEKUY!</h1>
                    <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br>tempor incididunt ut labore et dolore magna aliqua.</p>
                     --><a href="{{ url('/lapak') }}" class="btn btn-appoint btn-image img-location">Cari Lapak</a>
                     <!-- <a href="{{ action('PageController@lapak',2) }}" class="btn btn-default"> -->
                </div>  
            </div>
        </div>
    </div>
    </div>
    </section>
@endsection