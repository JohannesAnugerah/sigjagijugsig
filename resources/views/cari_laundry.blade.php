@extends('layouts.app')

@section('content')
    <section id="banner" class="banner">
        <div class="bg-color">
            <div class="container">
                <div class="row">
                    <div class="banner-info">
                        <div class="banner-text text-center">
                            <h1 class="white">NG-KOST</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="service" class="section-padding">

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <h2 class="ser-title">{{$title}}</h2>
                    <hr class="botm-line">

                    <div class="container">
                        <div class="row">
                            <form action="" id="search_laundry">
                                <div class="field is-grouped">
                                        <p class="control is-expanded">
                                            <input id="search_input" class="input" type="text" placeholder="Cari Laundry" name="key">
                                        </p>
                                        <p class="control">
                                            <a id="search_button" href="{{ action('PageController@search_laundry','')}}"><input class="btn btn-default" value="Cari" type="submit"></a>
                                        </p>
                                </div>
                            </form>
                            <script>
                                $('#search_input').on('input', function(e){
                                    var url = "{{ action('PageController@search_laundry', ':key')}}";
                                    $('#search_button').attr("href", url.replace(':key', $('#search_input').val()));
                                });

                                $('#search_laundry').on('submit',function(e){
                                    e.preventDefault();
                                    location.href =   $('#search_button').attr("href");
                                });
                            </script>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8">

                    <div class="service-info">
                        <div class="icon">
                            <i class="fa fa-search"></i>
                        </div>
                        <div class="icon-info">
                            <style>
                                .clickable{
                                    cursor: pointer;
                                }

                                .panel-heading div {
                                    margin-top: -18px;
                                    font-size: 15px;
                                }
                                .panel-heading div span{
                                    margin-left:5px;
                                }
                                .panel-body{
                                    display: none;
                                }
                            </style>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Laundry</h3>
                                </div>
                                <div class="panel-body">
                                </div>
                                <table class="table table-hover" id="dev-table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Laundry</th>
                                        <th>Harga (Rp)</th>
                                        <th>Jarak (km)</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $num = 1; ?>
                                    @if(!is_null($laundry))
                                        @foreach($laundry as $laun)
                                            <tr>
                                                <td>{{$num++}}</td>
                                                <td>{{$laun->nama}}</td>
                                                <td>{{$laun->harga}}</td>
                                                <td>{{$laun->distance}}</td>
                                                <td><a href="{{action('PageController@detail_laundry', $laun->id)}}">Detail</a></td>
                                            </tr>
                                        @endforeach
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection