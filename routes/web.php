<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');
Route::get('/lapak', 'PageController@lapak');
Route::get('/lapak2', 'PageController@lapak2');
Route::get('/lapak5', 'PageController@lapak5');
Route::get('/lapak10', 'PageController@lapak10');
Route::get('/lapak/all', 'PageController@all_lapak');
Route::get('/lapak/detail/{id}', 'PageController@detail_lapak');
Auth::routes();

Route::get('/about', 'PageController@about');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/test', 'TestController@lookup');
