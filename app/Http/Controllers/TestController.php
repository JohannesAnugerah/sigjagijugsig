<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use IP2LocationLaravel;			//use IP2LocationLaravel class

class TestController extends Controller
{
	//Create a lookup function for display
        public function lookup($ip){
		//Try query the geolocation information of 8.8.8.8 IP address
		$records = IP2LocationLaravel::get($ip);
		if ($records['latitude'] == 'Invalid IP address.') {
			$records = IP2LocationLaravel::get('127.0.0.0');
			return $records;
		}
		else {
			return $records;
		}

		//'127.0.0.0' '152.118.150.239'
		// public function retrieveLocationGoogleAPI()
	 //    {
	 //        $lokasi_lokasi = DB::table('lokasi')->get();
	 //        foreach ($lokasi_lokasi as $lokasi) {
	 //            $lat = $lokasi->latitude;
	 //            $long = $lokasi->longitude;
	 //            $json = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$long.'&key=AIzaSyCJHPBDwL_0bXgAEwjOHF-Vuii-tK_-2fY'), true);
	 //            $formatted_address = collect(collect(collect(collect($json)->first())->first()))->get('formatted_address');
	 //            $split_address = explode(', ', $formatted_address);
	 //            DB::table('lokasi')->where('id', $lokasi->id)->update(['alamat' => $split_address[0]]);
	 //            // try {
	 //            //     $kelurahan = $split_address[1];
	 //            //     $kecamatan =  $split_address[2];
	 //            //     $split_city = explode(' ', $split_address[3]);
	 //            //     $kota = $split_city[1];
	 //            //     DB::table('lokasi')->where('id', $lokasi->id)->update(['kelurahan' => $kelurahan]);
	 //            //     DB::table('lokasi')->where('id', $lokasi->id)->update(['kecamatan' => $kecamatan]);
	 //            //     DB::table('lokasi')->where('id', $lokasi->id)->update(['kota' => $kota]);
	 //            // }
	 //            // catch (\Exception $e) {
	 //            //     return $e->getMessage();
	 //            // }
	 //        }
	 //    }

		//ip ui '152.118.150.239'
		// echo 'IP Number             : ' . $records['ipNumber'] . "<br>";
		// echo 'IP Version            : ' . $records['ipVersion'] . "<br>";
		// echo 'IP Address            : ' . $records['ipAddress'] . "<br>";
		// echo 'Country Code          : ' . $records['countryCode'] . "<br>";
		// echo 'Country Name          : ' . $records['countryName'] . "<br>";
		// echo 'Region Name           : ' . $records['regionName'] . "<br>";
		// echo 'City Name             : ' . $records['cityName'] . "<br>";
		// echo 'Latitude              : ' . $records['latitude'] . "<br>";
		// echo 'Longitude             : ' . $records['longitude'] . "<br>";
		// echo 'Area Code             : ' . $records['areaCode'] . "<br>";
		// echo 'IDD Code              : ' . $records['iddCode'] . "<br>";
		// echo 'Weather Station Code  : ' . $records['weatherStationCode'] . "<br>";
		// echo 'Weather Station Name  : ' . $records['weatherStationName'] . "<br>";
		// echo 'MCC                   : ' . $records['mcc'] . "<br>";
		// echo 'MNC                   : ' . $records['mnc'] . "<br>";
		// echo 'Mobile Carrier        : ' . $records['mobileCarrierName'] . "<br>";
		// echo 'Usage Type            : ' . $records['usageType'] . "<br>";
		// echo 'Elevation             : ' . $records['elevation'] . "<br>";
		// echo 'Net Speed             : ' . $records['netSpeed'] . "<br>";
		// echo 'Time Zone             : ' . $records['timeZone'] . "<br>";
		// echo 'ZIP Code              : ' . $records['zipCode'] . "<br>";
		// echo 'Domain Name           : ' . $records['domainName'] . "<br>";
		// echo 'ISP Name              : ' . $records['isp'] . "<br>";
	}
}