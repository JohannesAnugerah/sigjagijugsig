<?php
/**
 * Created by PhpStorm.
 * User: Johannes
 * Date: 19/12/18
 * Time: 9.38 PM
 */
namespace App\Http\Controllers;
use App\Lapak;
use Illuminate\Support\Facades\App;
use Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Stevebauman\Location\Position;
use Stevebauman\Location\Facades\Location;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;


class PageController extends Controller{

    public function index() {
        
        // app('App\Http\Controllers\TestController')->retrieveLocationGoogleAPI();
       
            return view('index');
       
    }

    public function lapak(){
        if(!empty($_POST['latitude']) && !empty($_POST['longitude'])){
            //send request and receive json data by latitude and longitude
            $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($_POST['latitude']).','.trim($_POST['longitude']).'&sensor=false';
            $json = @file_get_contents($url);
            $data = json_decode($json);
            $status = $data->status;
            
            //if request status is successful
            if($status == "OK"){
                //get address from json data
                $location = $data->results[0]->formatted_address;
            }else{
                $location =  '';
            }
            
            //return address to ajax 
            echo $location;
        }
        
        $ip = $this->getIp();
        $lapak = DB::table('lapak')->get();
        $current_location = app('App\Http\Controllers\TestController')->lookup($ip);
        // $current_location= '-6.3645556','106.8288704';
        $lat = $current_location['latitude'];
        $lng = $current_location['longitude'];
        $regionName = $current_location['regionName'];
        $cityName = $current_location['cityName'];
        $countryName = $current_location['countryName'];

        $collection = collect([]);
        foreach ($lapak as $l) {
            $latitude = $l->latitude;
            $longitude = $l->longitude;
            $distance = 6371 * acos(cos(deg2rad($lat))*cos(deg2rad($latitude))*cos(deg2rad($longitude) - deg2rad($lng)) + sin(deg2rad($lat)) * sin(deg2rad($latitude)));
            DB::table('lapak')->where('id', $l->id)->update(['distance' => $distance]);
        }
        $nearest_lapak = DB::table('lapak')->orderBy('distance')->limit(25)->get();

        return view('lihat_daftar_lapak', [
            'title' => 'Welcome',
            'lapak' => $nearest_lapak,
            'ip' => $ip,
            'lat' => $lat,
            'lng' => $lng,
            'cityName' => $cityName,
            'regionName' => $regionName,
            'countryName' => $countryName
        ]);
    }

    public function lapak2(){
        if(!empty($_POST['latitude']) && !empty($_POST['longitude'])){
            //send request and receive json data by latitude and longitude
            $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($_POST['latitude']).','.trim($_POST['longitude']).'&sensor=false';
            $json = @file_get_contents($url);
            $data = json_decode($json);
            $status = $data->status;
            
            //if request status is successful
            if($status == "OK"){
                //get address from json data
                $location = $data->results[0]->formatted_address;
            }else{
                $location =  '';
            }
            
            //return address to ajax 
            echo $location;
        }
        
        $ip = $this->getIp();
        $lapak = DB::table('lapak')->get();
        $current_location = app('App\Http\Controllers\TestController')->lookup('152.118.150.239');
        // $current_location= '-6.3645556','106.8288704';
        $lat = $current_location['latitude'];
        $lng = $current_location['longitude'];
        $regionName = $current_location['regionName'];
        $cityName = $current_location['cityName'];
        $countryName = $current_location['countryName'];

        $collection = collect([]);
        foreach ($lapak as $l) {
            $latitude = $l->latitude;
            $longitude = $l->longitude;
            $distance = 6371 * acos(cos(deg2rad($lat))*cos(deg2rad($latitude))*cos(deg2rad($longitude) - deg2rad($lng)) + sin(deg2rad($lat)) * sin(deg2rad($latitude)));
            DB::table('lapak')->where('id', $l->id)->update(['distance' => $distance]);
        }
        $nearest_lapak = DB::table('lapak')->orderBy('distance')->limit(25)->get();

        return view('lihat_daftar_lapak2', [
            'title' => 'Welcome',
            'lapak' => $nearest_lapak,
            'ip' => $ip,
            'lat' => $lat,
            'lng' => $lng,
            'cityName' => $cityName,
            'regionName' => $regionName,
            'countryName' => $countryName
        ]);
    }

    public function lapak5(){
        if(!empty($_POST['latitude']) && !empty($_POST['longitude'])){
            //send request and receive json data by latitude and longitude
            $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($_POST['latitude']).','.trim($_POST['longitude']).'&sensor=false';
            $json = @file_get_contents($url);
            $data = json_decode($json);
            $status = $data->status;
            
            //if request status is successful
            if($status == "OK"){
                //get address from json data
                $location = $data->results[0]->formatted_address;
            }else{
                $location =  '';
            }
            
            //return address to ajax 
            echo $location;
        }
        
        $ip = $this->getIp();
        $lapak = DB::table('lapak')->get();
        $current_location = app('App\Http\Controllers\TestController')->lookup('152.118.150.239');
        // $current_location= '-6.3645556','106.8288704';
        $lat = $current_location['latitude'];
        $lng = $current_location['longitude'];
        $regionName = $current_location['regionName'];
        $cityName = $current_location['cityName'];
        $countryName = $current_location['countryName'];

        $collection = collect([]);
        foreach ($lapak as $l) {
            $latitude = $l->latitude;
            $longitude = $l->longitude;
            $distance = 6371 * acos(cos(deg2rad($lat))*cos(deg2rad($latitude))*cos(deg2rad($longitude) - deg2rad($lng)) + sin(deg2rad($lat)) * sin(deg2rad($latitude)));
            DB::table('lapak')->where('id', $l->id)->update(['distance' => $distance]);
        }
        $nearest_lapak = DB::table('lapak')->orderBy('distance')->limit(25)->get();

        return view('lihat_daftar_lapak5', [
            'title' => 'Welcome',
            'lapak' => $nearest_lapak,
            'ip' => $ip,
            'lat' => $lat,
            'lng' => $lng,
            'cityName' => $cityName,
            'regionName' => $regionName,
            'countryName' => $countryName
        ]);
    }

    public function lapak10(){
        if(!empty($_POST['latitude']) && !empty($_POST['longitude'])){
            //send request and receive json data by latitude and longitude
            $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($_POST['latitude']).','.trim($_POST['longitude']).'&sensor=false';
            $json = @file_get_contents($url);
            $data = json_decode($json);
            $status = $data->status;
            
            //if request status is successful
            if($status == "OK"){
                //get address from json data
                $location = $data->results[0]->formatted_address;
            }else{
                $location =  '';
            }
            
            //return address to ajax 
            echo $location;
        }
        
        $ip = $this->getIp();
        $lapak = DB::table('lapak')->get();
        $current_location = app('App\Http\Controllers\TestController')->lookup('152.118.150.239');
        // $current_location= '-6.3645556','106.8288704';
        $lat = $current_location['latitude'];
        $lng = $current_location['longitude'];
        $regionName = $current_location['regionName'];
        $cityName = $current_location['cityName'];
        $countryName = $current_location['countryName'];

        $collection = collect([]);
        foreach ($lapak as $l) {
            $latitude = $l->latitude;
            $longitude = $l->longitude;
            $distance = 6371 * acos(cos(deg2rad($lat))*cos(deg2rad($latitude))*cos(deg2rad($longitude) - deg2rad($lng)) + sin(deg2rad($lat)) * sin(deg2rad($latitude)));
            DB::table('lapak')->where('id', $l->id)->update(['distance' => $distance]);
        }
        $nearest_lapak = DB::table('lapak')->orderBy('distance')->limit(25)->get();

        return view('lihat_daftar_lapak10', [
            'title' => 'Welcome',
            'lapak' => $nearest_lapak,
            'ip' => $ip,
            'lat' => $lat,
            'lng' => $lng,
            'cityName' => $cityName,
            'regionName' => $regionName,
            'countryName' => $countryName
        ]);
    }

    public function detail_lapak($id){
        $lapak = collect(DB::table('lapak')->where('id', $id)->first());
        $id_lokasi = $lapak->get('lokasi_lapak');
        $lokasi_lapak = collect(DB::table('lokasi')->where('id', $id_lokasi)->first());
        $latitude = $lapak->get('latitude');
        $longitude = $lapak->get('longitude');
        $nama = $lapak->get('nama');
        $wilayah_kota =  $lapak->get('wilayah_kota');
        $alamat = $lapak->get('alamat');
        $nama_lokasi = $lapak->get('nama_lokasi');
        $masakan_1 = $lapak->get('masakan_1');
        $masakan_2 = $lapak->get('masakan_2');
        $waktu_buka = $lapak->get('waktu_buka');
        $wilayah = $lapak->get('wilayah');


        return view('lihat_rinci_lapak', [
            'nama' => $nama,
            'masakan_1' => $masakan_1,
            'masakan_2' => $masakan_2,
            'waktu_buka' => $waktu_buka,
            'alamat' => $alamat,
            'wilayah' => $wilayah,
            'wilayah_kota' => $wilayah_kota,
            'nama_lokasi' => $nama_lokasi,
            'latitude' => $latitude,
            'longitude' => $longitude
        ]);
    }

    public function all_lapak(){
        // $ip = $this->getIp();
        // $lapak = DB::table('lapak')->orderBy('distance')->get();
        // $current_location = app('App\Http\Controllers\TestController')->lookup($ip);
        // $lat = $current_location['latitude'];
        // $lng = $current_location['longitude'];

        $ip = $this->getIp();
        $lapak = DB::table('lapak')->get();
        $current_location = app('App\Http\Controllers\TestController')->lookup('127.0.0.0');
        $lat = $current_location['latitude'];
        $lng = $current_location['longitude'];
        $regionName = $current_location['regionName'];
        $cityName = $current_location['cityName'];
        $countryName = $current_location['countryName'];

        $collection = collect([]);
        foreach ($lapak as $l) {
            $latitude = $l->latitude;
            $longitude = $l->longitude;
            $distance = 6371 * acos(cos(deg2rad($lat))*cos(deg2rad($latitude))*cos(deg2rad($longitude) - deg2rad($lng)) + sin(deg2rad($lat)) * sin(deg2rad($latitude)));
            DB::table('lapak')->where('id', $l->id)->update(['distance' => $distance]);
        }
        $nearest_lapak = DB::table('lapak')->orderBy('distance')->get();

        return view('lihat_daftar_lapak', [
            'title' => 'Welcome',
            'lapak' => $nearest_lapak,
            'ip' => $ip,
            'lat' => $lat,
            'lng' => $lng,
            'cityName' => $cityName,
            'regionName' => $regionName,
            'countryName' => $countryName
        ]);
    }

    public function about() {
        return view('lihat_tentang', ['about' => 'Welcome']);
    }

    public function search_lapak($key = null){
//        dd($key);
        if(is_null($key)) {
            $lapak = null;
        } else {
            $id_lokasi = DB::table('lokasi')->where('kelurahan', 'like', '%' . $key . '%')->orWhere('kecamatan', 'like', '%' . $key . '%')->orWhere('kota', 'like', '%' . $key . '%')->pluck('id')->all();
            $lapak = DB::table('lapak')->whereIn('lokasi_lapak', $id_lokasi)->get();
        }
        return view('cari_lapak', [
            'title' => 'Cari',
            'lapak' => $lapak
        ]);
    }

    public function getIPAddress() {
        return Request::ip();
    }

    public function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }
}