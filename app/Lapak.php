<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lapak extends Model
{
    protected $table = 'lapak';
    protected $fillable = [
        'wilayah_kota', 'nama', 'alamat', 'wilayah', 'nama_lokasi', 'masakan_1', 'masakan_2', 'waktu_buka', 'latitude', 'longitude', 'id'
    ];
    
    public function getTable()
    {
        return $this->table;
    }

    public function has($attribute)
    {
        return in_array($attribute, $this->fillable);
    }
}
